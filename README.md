Pan Genomic Reference Variant Calling Pipeline
=========

The pipeline consists of the following components:

* Pan-Genome Index .

* Weighted Matrix Generator.

* Heaviest path finder.

* VCF - Normalizator

Each of these can be used independently, and more info is provided in each folder. 
The pipeline also uses a standard pipeline for variation calling. 
We provide example usage with BWA+Samtools, and also scripts to use BWA+GATK,
although you need to manually install GATK if you want to use the latter.


Usage
-----------
To perform variant calling using a pan-genomic reference we provide the two following scripts:

&nbsp;

**pipeline_simple.sh**

Parameters:

1.- Pan-Genomic reference as a multiple sequence alignment in .maligned format

2.- NGS reads in fq format

Output:

variations.vcf 

&nbsp;

**pipeline_parametric.sh**

Parameters:

1.- Pan-Genomic reference as a multiple sequence alignment in .maligned format

2.- NGS reads in fq format.

3.- Working folder

Output:

variations.vcf 

If your are interested in the intermediate files, such as the ad-hoc reference
you they are left in the working folder