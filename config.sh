# External tool to perform variation calling once we have the ad-hoc reference:
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

### The following runs MSA_{base} method, which simulates the pan genome index by using BWA on each sequence in the pan-genome
#PG_INDEX_DIR="${DIR}/components/pan_genome_index_simulated/"
### This uses CHIC aligner to index the pan-genome
PG_INDEX_DIR="${DIR}/components/pan_genome_index_real/"

### BWA+GATK  var-call. Make sure you have installed GATK
#EXT_VCF_TOOL_SH=${DIR}/ext_var_call_pipelines/BwaGATK/pipeline.sh
### BWA+Samtools var-call 
EXT_VCF_TOOL_SH=${DIR}/ext_var_call_pipelines/BwaSamtoolsSNPs/pipeline.sh
utils_assert_file_exists ${EXT_VCF_TOOL_SH}
