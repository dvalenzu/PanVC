#!/usr/bin/env bash
set -o errexit
set -o nounset

##  
##
##

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

## External code:
TARGET=components/pan_genome_index_simulated/ext/bwa-0.7.12/
echo "********************************"
echo "Entering ${TARGET}"
echo "********************************\n"
cd ${TARGET}
make clean;
make;
cd ${DIR} 

## External code:
TARGET=components/pan_genome_index_real/ChicAligner-beta
echo "********************************"
echo "Entering ${TARGET}"
echo "********************************\n"
cd ${TARGET}
#make clean;
make;
cd ${DIR} 

## External code:
TARGET=components/pan_genome_index_real/msa2fastas
echo "********************************"
echo "Entering ${TARGET}"
echo "********************************\n"
cd ${TARGET}
#make clean;
make;
cd ${DIR} 

TARGET=components/heaviest_paths/src/libcds/
echo "********************************"
echo "Entering ${TARGET}"
echo "********************************\n"
cd ${TARGET}
make clean;
make;
cd ${DIR} 

TARGET=components/normalize_vcf/jvarkit/
echo "********************************"
echo "Entering ${TARGET}"
echo "********************************\n"
cd ${TARGET}
make clean;
make biostar94573
cd ${DIR} 

### Own code:
TARGET=components/heaviest_paths/src/
echo "********************************"
echo "Entering ${TARGET}"
echo "********************************\n"
cd ${TARGET}
make clean;
make;
cd ${DIR} 

TARGET=components/normalize_vcf/projector
echo "********************************"
echo "Entering ${TARGET}"
echo "********************************\n"
cd ${TARGET}
make clean;
make;
cd ${DIR} 

TARGET=ext_var_call_pipelines/
echo "********************************"
echo "Entering ${TARGET}"
echo "********************************\n"
cd ${TARGET}
./compile.sh
cd ${DIR} 

echo ""
echo "Internal and external libraries successfully compiled"
echo "You may want to run ./test_simple.sh and ./test_parametric.sh"
echo "to verify that the pipeline is ready to use."


