#!/usr/bin/env bash
set -o errexit
set -o nounset

if [ "$#" -ne 3 ]; then
  echo "Illegal number of parameters. I should be called from the pipeline script, with 9 params instead of $# " 
  exit 33
fi
  echo "OK Params"

REFERENCE_FILE=${1}

READS_FILE=${2}

OUTPUT_FILE=${3}

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "${DIR}/../ext/set_env.sh"

if [ ! -f "${REFERENCE_FILE}" ]; then
  echo "File '${REFERENCE_FILE}'not found!. File: '${0}'"
  exit 33
fi
if [ ! -f "${READS_FILE}" ]; then
  echo "File '${READS_FILE}'not found!. File: '${0}'"
  exit 33
fi

if [ ! -f "${BWA_BIN}" ]; then
  echo "File '${BWA_BIN}'not found!. File: '${0}'"
  exit 33
fi
if [ ! -f "${SAMTOOLS_BIN}" ]; then
  echo "File '${SAMTOOLS_BIN}'not found!. File: '${0}'"
  exit 33
fi
if [ ! -f "${BCFTOOLS_BIN}" ]; then
  echo "File '${BCFTOOLS_BIN}'not found!. File: '${0}'"
  exit 33
fi
if [ ! -f "${VCFUTILS_BIN}" ]; then
  echo "File '${VCFUTILS_BIN}'not found!. File: '${0}'"
  exit 33
fi
if [ ! -f "${PICARD_JAR}" ]; then
  echo "File '${PICARD_JAR}'not found!. File: '${0}'"
  exit 33
fi
if [ ! -f "${GATK_JAR}" ]; then
  echo "File '${GATK_JAR}'not found!. File: '${0}'"
  exit 33
fi


### CLEAN UP AND FORMAT INPUT
rm -rf tmp_data
mkdir tmp_data

REF=$REFERENCE_FILE
REF_FOLDER=$(dirname "${REFERENCE_FILE}")
REF_BASE=`basename $REFERENCE_FILE`
REF_PREFIX=${REF_BASE%.*}
REF_DICT=${REF_FOLDER}/${REF_PREFIX}.dict
READS=$READS_FILE


echo 
echo "########################## "
echo "    MAP AND DEDUP  :    "
echo "#########################  "
echo
echo "Ref:    $REFERENCE_FILE"
echo "prefix: $REF_PREFIX"

#INDEX_FILE=${REFERENCE_FILE}.bwt
#if [ ! -f "${INDEX_FILE}" ]; then
#  echo "Index not found, indexing reference... '${INDEX_FILE}'not found!"
#  ${BWA_BIN} index $REFERENCE_FILE 
#else
#  echo "Index file  '${INDEX_FILE}' found, reusing it."
#fi
echo "########################## "
echo "    INDEX REF (with timer):    "
echo "#########################  "
time ${BWA_BIN} index  ${REF}
#${BWA_BIN} index ${REF}
# -a allows whole genome.
${SAMTOOLS_BIN} faidx ${REF}


rm -f ${REF_DICT}  
java -jar ${PICARD_JAR} CreateSequenceDictionary REFERENCE=${REF} OUTPUT=${REF_DICT}

echo "########################## "
echo "    READ ALIGNMENT (with timer):    "
echo "#########################  "
#${BWA_BIN} samse ${REF} ${READS} > aln-pe.sam
#${BWA_BIN} sampe ${REF} ${READS} > aln-pe.sam
time ${BWA_BIN}  mem -M -R "@RG\tID:group1\tSM:sample1\tPL:illumina\tLB:lib1\tPU:unit1" ${REF} ${READS} > tmp_data/aligned_reads.sam
#The -M flag causes BWA to mark shorter split hits as secondary (essential for Picard compatibility).

java -jar ${PICARD_JAR} SortSam                                INPUT=tmp_data/aligned_reads.sam OUTPUT=tmp_data/sorted_reads.bam SORT_ORDER=coordinate 
#java -jar ${PICARD_JAR} SortSam VALIDATION_STRINGENCY=LENIENT  INPUT=tmp_data/aligned_reads.sam OUTPUT=tmp_data/sorted_reads.bam SORT_ORDER=coordinate 

java -jar ${PICARD_JAR} MarkDuplicates INPUT=tmp_data/sorted_reads.bam OUTPUT=tmp_data/dedup_reads.bam METRICS_FILE=tmp_data/metrics.txt

java -jar ${PICARD_JAR} BuildBamIndex INPUT=tmp_data/dedup_reads.bam 

echo 
echo "########################## "
echo "    INDEL REALIGNMENT:    "
echo "#########################  "
echo

java -jar ${GATK_JAR} -T RealignerTargetCreator -R ${REF} -I tmp_data/dedup_reads.bam   -o tmp_data/target_intervals.list 
#java -jar ${GATK_JAR} -T RealignerTargetCreator -R ${REF} -I tmp_data/dedup_reads.bam -L 20  -o tmp_data/target_intervals.list 
# We don't assume known gold indels.
# Also we remove the -L 20 which was for resticting to chromosome 20 (and poorly documented).


#java -jar ${GATK_JAR} -T IndelRealigner -R ${REF} -I tmp_data/dedup_reads.bam -targetIntervals tmp_data/target_intervals.list -known gold_indels.vcf -o tmp_data/realigned_reads.bam 
java -jar ${GATK_JAR} -T IndelRealigner -R ${REF} -I tmp_data/dedup_reads.bam -targetIntervals tmp_data/target_intervals.list                        -o tmp_data/realigned_reads.bam 

echo 
echo "########################## "
echo "    BASE RECALIBRATION:    "
echo "#########################  "
echo
echo "skipped..."
echo


# We Skip that for now, as we dont have a knownSites.
# For that case, the documentation suggest call SNP variants once, and use those as the known sites.
# And repeat until convergence ... :S
#java -jar GenomeAnalysisTK.jar -T BaseRecalibrator -R reference.fa -I realigned_reads.bam -knownSites dbsnp.vcf -knownSites gold_indels.vcf -o recal_data.table 

echo 
echo "########################## "
echo "    CALL VARIANTS:    "
echo "#########################  "
echo

java -jar ${GATK_JAR} -T HaplotypeCaller -R ${REF} -I tmp_data/realigned_reads.bam --genotyping_mode DISCOVERY -stand_emit_conf 10 -stand_call_conf 30 -o tmp_data/raw_variants.vcf
#java -jar ${GATK_JAR} -T HaplotypeCaller -R ${REF} -I tmp_data/realigned_reads.bam --genotyping_mode DISCOVERY -stand_emit_conf 5 -stand_call_conf 15 -o tmp_data/raw_variants.vcf

mv tmp_data/raw_variants.vcf $OUTPUT_FILE
mv tmp_data/   "${OUTPUT_FILE}_data_dir"
