#!/usr/bin/env bash
set -o errexit
set -o nounset

rm -f output.vcf
rm -rf tmp_data
