#!/usr/bin/env bash
set -o errexit
set -o nounset

rm output.vcf
rm -rf tmp_data
