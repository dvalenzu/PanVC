#!/usr/bin/env bash
set -o errexit
set -o nounset

if [ "$#" -ne 3 ]; then
  echo "Illegal number of parameters. I should be called from the pipeline script, with 9 params instead of $# " 
  exit 33
fi
  echo "OK Params"

REFERENCE_FILE=${1}

READS_FILE=${2}

OUTPUT_FILE=${3}

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "${DIR}/../ext/set_env.sh"

if [ ! -f "${REFERENCE_FILE}" ]; then
  echo "File '${REFERENCE_FILE}'not found!. File: '${0}'"
  exit 33
fi
if [ ! -f "${READS_FILE}" ]; then
  echo "File '${READS_FILE}'not found!. File: '${0}'"
  exit 33
fi

if [ ! -f "${BWA_BIN}" ]; then
  echo "File '${BWA_BIN}'not found!. File: '${0}'"
  exit 33
fi
if [ ! -f "${SAMTOOLS_BIN}" ]; then
  echo "File '${SAMTOOLS_BIN}'not found!. File: '${0}'"
  exit 33
fi
if [ ! -f "${BCFTOOLS_BIN}" ]; then
  echo "File '${BCFTOOLS_BIN}'not found!. File: '${0}'"
  exit 33
fi
if [ ! -f "${VCFUTILS_BIN}" ]; then
  echo "File '${VCFUTILS_BIN}'not found!. File: '${0}'"
  exit 33
fi


## TODO: check format, if ref is not fasta or reads are not fastq, throw an error.

# TODO: check all files, not one.
# Check they are not empty.
INDEX_FILE=${REFERENCE_FILE}.bwt
if [ ! -f "${INDEX_FILE}" ]; then
  echo "Index not found, indexing reference... '${INDEX_FILE}'not found!"
  ${BWA_BIN} index $REFERENCE_FILE 
else
  echo "Index file  '${INDEX_FILE}' found, reusing it."
fi

#${BWA_BIN} samse tmp_data/reference_file.fasta tmp_data/reads_file.fasta > aligned.sam
#${BWA_BIN} sampe tmp_data/reference_file.fasta tmp_data/reads_file.fasta > aligned.sam
${BWA_BIN} mem $REFERENCE_FILE $READS_FILE > aligned.sam
${SAMTOOLS_BIN} view -Sb aligned.sam > aligned.bam
${SAMTOOLS_BIN} sort -f aligned.bam  sorted-alns2.bam
${SAMTOOLS_BIN} mpileup -uf $REFERENCE_FILE  sorted-alns2.bam | ${BCFTOOLS_BIN} view -bvcg - > var.raw.bcf 
${BCFTOOLS_BIN} view var.raw.bcf | ${VCFUTILS_BIN} varFilter -D100 > var.flt.vcf

mv var.flt.vcf $OUTPUT_FILE
rm var.*cf
rm *am
