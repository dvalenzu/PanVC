# Root directory of the pan_genome_index
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

## Flags:
BWA_FLAGS_THREADS="-t 7"
REUSE_INDEX=1
REUSE_SAMS=1


### scripts, files, etc.
BWA_BIN=${DIR}/ext/bwa-0.7.12/bwa
GAP_POSITIONS_PY=${DIR}/scripts/gap_positions.py 
