# Root directory of the pan_genome_index
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

## Flags:
#BWA_FLAGS_THREADS="-t 7"  not used yet. CHIC needs to accept bwa parameters.
REUSE_INDEX=1
REUSE_SAMS=1
##CHIC_ALIGNER PARAM
MAX_QUERY_LEN=120 

### scripts, files, etc.
MSA_2_FASTAS=${DIR}/msa2fastas/msa2fastas
CHIC_INDEX_BIN=${DIR}/ChicAligner-beta/src/chic_index
CHIC_ALIGN_BIN=${DIR}/ChicAligner-beta/src/chic_align
GAP_POSITIONS_PY=${DIR}/scripts/gap_positions.py 
