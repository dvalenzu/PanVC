CHICO: Compressed Hybrid Index for repetitive Collections


Main features::
===========
* Scales up to Terabyte-sized data
* Lightweight construction: the user can specify the maximum RAM to be used
* Any LZ77 parsing can be used to build the index. Built-in options include 
  in-memory and external memory greedy LZ77. A parallel version of RLZ is also
  included

COMPILE
=======

First you need to compile the external tools, to do that:

  cd ext;
  ./compile.sh;
  cd ..

Then you can compile the source code:

  cd ./src;
  make;
  ..

Ussage
======

To index a file:

./build_index [OPTIONS] INPUT_FILE MAX_QUERY_LEN

To query an index:

./load_index [OPTIONS] index_basename patterns_filename


