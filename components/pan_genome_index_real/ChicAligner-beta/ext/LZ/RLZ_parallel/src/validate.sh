#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"
source "./utils_validate.sh"

LIST="data_2 data_3 data_4 data_5 data_6 data_7 data_8"
for FOLDER_NAME in ${LIST}; do
  INPUT_FILE="/home/dvalenzu/PHD/repos_code/CHICO/test_scripts/bio_data/${FOLDER_NAME}/genome.fa"
  validate_fasta ${INPUT_FILE}  > test_${FOLDER_NAME}.log
done
utils_success_exit
