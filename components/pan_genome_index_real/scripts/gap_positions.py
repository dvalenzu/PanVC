import sys
def findOccurrences(s, ch):
  return [i for i, letter in enumerate(s) if letter == ch]

def get_gaps_positions(file_name):
  f = open(file_name);
  n_lines=0
  for line in f:
    if(n_lines != 0):
      print 'number of lines bigger than one. error!'
      sys.exit();
    occs = findOccurrences(line, '-')
    n_lines = 1;
  for p in occs:
    print(p)
  f.close();

n_args = len(sys.argv);
if(n_args != 2):
  print 'Number of arguments:', n_args, 'arguments, is incorrect:'
  sys.exit();
FILE_NAME=sys.argv[1];
get_gaps_positions(FILE_NAME);
