#!/usr/bin/env bash
set -o errexit
set -o nounset

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source ${DIR}/config.sh
source ${DIR}/../../utils.sh

if [ "$#" -ne 3 ]; then
  echo "Illegal number of parameters. I should be called from the pipeline script, with 2 params instead of $# " 
  exit 33
else
  echo "OK Params"
fi

utils_assert_file_exists ${CHIC_ALIGN_BIN}

PAN_REFERENCE_FILE=${1}
READS_FILE=${2}
OUTPUT_FOLDER=${3}
utils_assert_file_exists ${PAN_REFERENCE_FILE}
utils_assert_file_exists ${READS_FILE}
# Output folder may not exist, in that case we will create it.

#############################################
#### Computing info from reference...
TMP_FOLDER=$(dirname "${PAN_REFERENCE_FILE}")
TMP_BASE=`basename $PAN_REFERENCE_FILE`
TMP_PREFIX=${TMP_BASE%.*}

PAN_REFERENCE_FOLDER=${TMP_FOLDER}/${TMP_PREFIX}.index/

N_REFS=$( zcat ${PAN_REFERENCE_FILE}  | tail -n1 | wc -c)
N_REFS=`expr $N_REFS - 1`
#############################################

## TODO: replace check directory and instead, check every file exists.
## Then, it can be done always.
if [ -d ${OUTPUT_FOLDER} ]; then
  if [ ${REUSE_SAMS} -eq 1 ]; then
    echo "Folder-Sams '${OUTPUT_FOLDER}' and flag REUSE_SAMS is set.
    we will not align the reads again. File: '${0}'"
    exit 0
  fi
  echo "Using an existing output folder to store sam files"
else
  echo "Output folder: ${OUTPUT_FOLDER} does not exists. Creating it ..."
  mkdir -p ${OUTPUT_FOLDER}
fi

SEQUENCE_ALL_FILE=$PAN_REFERENCE_FOLDER/recombinant.all.fa
SAM_ALL_PLAIN_FILE=${OUTPUT_FOLDER}/mapped_reads_all.sam
#TODO: I need to give the flag threads to the aligner...
#$CHIC_ALIGN_BIN  $BWA_FLAGS_THREADS $SEQUENCE_FILE $READS_FILE | gzip > $SAM_FILE
time ${CHIC_ALIGN_BIN}  --secondary=LZ $SEQUENCE_ALL_FILE $READS_FILE --output=$SAM_ALL_PLAIN_FILE

echo "Splitting into many SAM FILES:"
for (( CURRENT_REFERENCE=1; CURRENT_REFERENCE<=${N_REFS}; CURRENT_REFERENCE++ ))
do
  # Make the calls
  SAM_FILE=${OUTPUT_FOLDER}/mapped_reads_to${CURRENT_REFERENCE}.sam.gz
  cat ${SAM_ALL_PLAIN_FILE} | grep -P "PG_REF_${CURRENT_REFERENCE}\t" | gzip > ${SAM_FILE}
done

echo "SAM files generated"
