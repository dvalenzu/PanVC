#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <assert.h>
#include <stdlib.h> 
//  
//  Beware: If output files already exists it will APPEND ! 
//  This is a bug.
//
using std::cerr;
using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;

void flush_lines(vector<string> data, vector<string> names);
void flush_lines(vector<string> data, vector<string> names) {
  vector<string> out;
  out.reserve(names.size());
  for (size_t i = 0; i < names.size(); i++) {
    out.push_back(string(data.size(),'X'));
  }
  for (size_t i = 0; i < data.size(); i++) {
    assert(data[i].size() == names.size());
    for (size_t j = 0; j < names.size(); j++) {
      out[j][i] = data[i][j];
    }  
  }
  for (size_t i = 0; i < names.size(); i++) {
    ofstream ofs(names[i], std::ios_base::app);
    if (!ofs.is_open()) {
      cerr << "Problem opening " << names[i] << endl;
    }
    ofs << out[i];
    ofs.close();
  }
}

vector<string> init_filenames(int n) {
  vector<string> ans(n);
  for (int i = 0; i < n; i++) {
    ans[i] = "genome_" + std::to_string (i+1);  // 1-based for consistency with the pipeline
    ans[i] = "recombinant.n" + std::to_string (i+1) + ".full";  // 1-based for consistency with the pipeline
  
  }
  return ans;
}

// we assume all the lines have the same number of characters.
int main(int argc, char *argv[]) {
  if (argc != 2 ) {
    cout << "Usage: " << endl;
    cout << " " << argv[0] << " <source file>" << endl;
    exit(1);
  }
  ifstream input(argv[1]);


  string line;
  getline(input, line);
  int n_rows = line.size();
  cout << "N rows: "<< n_rows << endl; 
  // create n_rows file names.

  // Return to position before "Read line".
  input.seekg(0);


//////
  vector<string> data; 
  vector<string> file_names = init_filenames(n_rows); 
  size_t max_lines = (1<<20);  // 1 Mega lines * 1.000 chars ~ 1GB
  cerr << "Buffering lines: " << max_lines << endl;
  while (getline(input, line)) {
    data.push_back(line);
    if (data.size() >= max_lines) {
      flush_lines(data, file_names);
      data.clear();
      cout << "partial flush successfull..." << endl;
    }
  } 
  flush_lines(data, file_names);
  cout << "Success." << endl;
}
