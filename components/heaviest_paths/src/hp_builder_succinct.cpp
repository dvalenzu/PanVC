/* Copyright (C) 2013, Dauiniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <algorithm>
#include "./hp_builder_succinct.h"
#include "./succinct_cube.h"
#include "./cube.h"
#include "./partial_cube.h"
#include "./plain_cube.h"
#include "./score_matrix.h"
#include "./basic.h"


SuccinctHPBuilder::SuccinctHPBuilder() {
}

size_t * SuccinctHPBuilder::RestrictedHeaviestPath(ScoreMatrix * scores,
                                                   size_t changes_allowed,
                                                   score_t max_score,
                                                   score_t  * total_weight,
                                                   size_t * space_required) {
  size_t length = scores->GetLength();
  size_t n_seqs = scores->GetNSeqs();
  assert(changes_allowed < length);
  score_t default_score = 0;
  SuccinctCube succinct_cube(n_seqs,
                             length,
                             changes_allowed + 1,
                             max_score,
                             default_score);
  *space_required = succinct_cube.SpaceRequirementBits();
  ComputeCube(scores, &succinct_cube, changes_allowed);
  succinct_cube.PrepareForQueries();
  // PrintLevel(&matrix, 0);
  // PrintLevel(&matrix, 1);
  return GetFullPath(&succinct_cube, changes_allowed, total_weight);
}
