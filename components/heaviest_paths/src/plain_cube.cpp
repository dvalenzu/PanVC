/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include "./plain_cube.h"
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <vector>
#include <algorithm>
#include "./basic.h"
#include "./debug.h"

PlainCube::PlainCube(size_t x,
                     size_t y,
                     size_t z,
                     score_t _max_score,
                     score_t init) {
  n_seqs = x;
  length = y;
  depth = z;
  max_score = _max_score;
  m_data = std::vector<score_t>(x*y*z, init);
}

void PlainCube::Set(size_t x, size_t y, size_t z, score_t val) {
  assert(x < n_seqs);
  assert(y < length);
  assert(z < depth);
  m_data.at(x + y * n_seqs + z * n_seqs * length) = val;
}

score_t PlainCube::Get(size_t x, size_t y, size_t z) {
  assert(x < n_seqs);
  assert(y < length);
  assert(z < depth);
  return m_data.at(x + y * n_seqs + z * n_seqs * length);
}

size_t PlainCube::SpaceRequirementBits() {
  return ((n_seqs * depth) *
          length * bits((uint)max_score));
}

