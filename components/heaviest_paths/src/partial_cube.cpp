/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include "./partial_cube.h"
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <vector>
#include <algorithm>
#include "./basic.h"
#include "./debug.h"

PartialCube::PartialCube(size_t x,
                         size_t y,
                         size_t z,
                         size_t n_layers,
                         score_t _max_score,
                         score_t init) {
  n_seqs = x;
  length = y;
  depth = z;
  n_layers--;
  // beacuse we also store one persistent layer in previous_layer_data
  mini_cube_layers = n_layers;
  first_persistent_layer = length - n_layers;
  max_score = _max_score;
  default_score = init;
  current_layer_data = new score_t[n_seqs * depth];
  previous_layer_data = new score_t[n_seqs * depth];
  for (uint i = 0; i < n_seqs * depth; i++) {
    previous_layer_data[i] = default_score;
    current_layer_data[i] = default_score;
  }
  current_y = 0;
}

void PartialCube::Set(size_t x, size_t y, size_t z, score_t val) {
  assert(x < n_seqs);
  assert(y < length);
  assert(z < depth);
  if (current_y == first_persistent_layer) {
    assert(y >= current_y);
    if (m_data.size() == 0) {
      // the case where we behave as a plain cube:
      m_data = std::vector<score_t>(n_seqs * mini_cube_layers * depth,
                                    default_score);
      delete[] current_layer_data;
      // we keep previous layer for consistence, but will not be used.
    }

    size_t relative_y = y - first_persistent_layer;
    m_data.at(x + relative_y * n_seqs + z * n_seqs * mini_cube_layers) = val;
    return;
  } else {
    if (y < first_persistent_layer) {
      if (y == (current_y + 1)) {
        UpdateLayers();
        current_y++;
      }
      current_layer_data[x + z *n_seqs] = val;
      return;
    } else {
      assert(y == first_persistent_layer &&
             current_y == (first_persistent_layer - 1));
      delete[] previous_layer_data;
      previous_layer_data = current_layer_data;
      current_y++;
      m_data = std::vector<score_t>(n_seqs * mini_cube_layers * depth,
                                    default_score);
      size_t relative_y = y - first_persistent_layer;
      m_data.at(x + relative_y * n_seqs + z * n_seqs * mini_cube_layers) = val;
      return;
    }
    assert(0);
  }
  assert(0);
}

void PartialCube::UpdateLayers() {
  delete[] previous_layer_data;
  previous_layer_data = current_layer_data;
  current_layer_data = new score_t[n_seqs * depth];
  for (uint i = 0; i < n_seqs * depth; i++) {
    current_layer_data[i] = default_score;
  }
}

score_t PartialCube::Get(size_t x, size_t y, size_t z) {
  assert(x < n_seqs);
  assert(y < length);
  assert(z < depth);
  if (current_y == first_persistent_layer) {
    if (y >= current_y) {
      size_t relative_y = y - first_persistent_layer;
      return m_data.at(x + relative_y * n_seqs + z * n_seqs * mini_cube_layers);
    } else if (y == current_y - 1) {
      return previous_layer_data[x + z *n_seqs];
    }
    assert(0);
  } else {
    if (y == current_y)
      return current_layer_data[x + z *n_seqs];
    if (y+1 == current_y)
      return previous_layer_data[x + z *n_seqs];
    assert(0);
  }
}

size_t PartialCube::SpaceRequirementBits() {
  return ((n_seqs * depth) *
          length * bits((uint)max_score));
}

PartialCube::~PartialCube() {
  if (current_y < first_persistent_layer) {
    delete[] previous_layer_data;
    delete[] current_layer_data;
  } else {
    delete[] previous_layer_data;
  }
}

