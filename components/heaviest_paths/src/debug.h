/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef SRC_DEBUG_H_
#define SRC_DEBUG_H_

#include <iostream>
#include "./basic.h"

class Debug {
 public:
  template<typename TYPE>
static void printArray(TYPE *A, size_t length) {
  if (length == 0) {
    std::cout << "[]";
    return;
  }
  //std::cout << "[";
  for (size_t i = 0; i < length-1; i++) {
    std::cout << A[i] << "-";
  }
  std::cout << A[length-1] << "]\n";
}

template<typename TYPE>
static bool compareArrays(TYPE *A, TYPE  * B, size_t length) {
  for (size_t i = 0; i < length; i++) {
    if (A[i] != B[i]) {
      std::cout << "Diff in pos:" << i << std::endl;
      return false;
    }
  }
  return true;
}
};
#endif
