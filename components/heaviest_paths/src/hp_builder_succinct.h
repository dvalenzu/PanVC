/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef SRC_SUCCINCT_HEAVIEST_PATH_H_
#define SRC_SUCCINCT_HEAVIEST_PATH_H_ 
#include <SequenceBuilder.h>
#include "./succinct_cube.h"
#include "./plain_cube.h"
#include "./score_matrix.h"
#include "./hp_builder.h"

class SuccinctHPBuilder : public HeaviestPath {
 public:
  SuccinctHPBuilder();
  size_t * RestrictedHeaviestPath(ScoreMatrix * scores,
                                  size_t changes_allowed, 
                                  score_t max_score, 
                                  score_t  * total_weight,
                                  size_t * bytes_required);
};
#endif
