/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <stdarg.h>
#include <cstdio>
#include <sstream>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include "./debug.h"

/*
template<typename TYPE>
void Debug::printArray(TYPE *A, size_t length) {
  if (length == 0) {
    std::cout << "[]";
    return;
  }
  std::cout << "[";
  for (size_t i = 0; i < length-1; i++) {
    //printf("%lu|", A[i]);
    std::cout << "|" << A[i] << "|";
  }
  std::cout << A[length-1] << "]";
}
*/
