/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include "./succinct_cube.h"
#include <BitSequence.h>
#include <BitSequenceRG.h>
#include <libcdsBitString.h>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <vector>
#include <algorithm>
#include "./basic.h"
#include "./debug.h"

SuccinctCube::SuccinctCube(size_t x,
                           size_t y,
                           size_t z,
                           score_t _max_score,
                           score_t init) {
  n_seqs = x;
  length = y;
  depth = z;
  max_score = _max_score;
  current_layer_data = new score_t[n_seqs * depth];
  previous_layer_data = new score_t[n_seqs * depth];
  bitmaps_data = new BitString*[n_seqs * depth];

  default_score = init;
  for (uint i = 0; i < n_seqs * depth; i++) {
    bitmaps_data[i] = new BitString(length + (uint)max_score + 1);
    previous_layer_data[i] = default_score;
    current_layer_data[i] = default_score;
  }
  current_y = 0;
  is_modifiable = true;
}

void SuccinctCube::UpdateBitmaps(size_t layer_y) {
  for (size_t x = 0; x < n_seqs; x++) {
    for (size_t z = 0; z < depth; z++) {
      int curr_delta = 0;
      if (layer_y > 0) {
        curr_delta = current_layer_data[x + z*n_seqs] -
            previous_layer_data[x + z*n_seqs];
        assert(curr_delta >= 0);
      }
      size_t bitpos = (layer_y + (uint)current_layer_data[x + z * n_seqs]);
      if (bitpos >= bitmaps_data[x + z * n_seqs]->getLength()) {
        // "realloc"
        BitString * old = bitmaps_data[x + z * n_seqs];

        size_t new_length = 2 * old->getLength();
        while (new_length <= bitpos) {
          fprintf(stderr,
                  "Warning: If max_score was properly set, we shouldnt be here.\n");
          new_length *= 2;
        }

        BitString * tmp = new BitString(new_length);
        for (size_t i = 0; i < old->getLength(); i++) {
          tmp->setBit(i, old->getBit(i));
        }
        delete(old);
        bitmaps_data[x + z * n_seqs] = tmp;
      }
      bitmaps_data[x + z * n_seqs]->setBit(bitpos);
    }
  }
}

void SuccinctCube::UpdateLayers() {
  delete[] previous_layer_data;
  previous_layer_data = current_layer_data;
  current_layer_data = new score_t[n_seqs * depth];
  for (uint i = 0; i < n_seqs * depth; i++) {
    current_layer_data[i] = default_score;
  }
}

void SuccinctCube::Set(size_t x, size_t y, size_t z, score_t val) {
  assert(x < n_seqs);
  assert(y < length);
  assert(z < depth);

  assert(is_modifiable);
  assert(y == current_y || y == current_y + 1);

  if (y == current_y + 1) {
    UpdateBitmaps(current_y);
    UpdateLayers();
    current_y++;
  }
  assert(y == current_y);
  current_layer_data[x + z *n_seqs] = val;
}

score_t SuccinctCube::Get(size_t x, size_t y, size_t z) {
  assert(x < n_seqs);
  assert(y < length);
  assert(z < depth);
  if (is_modifiable) {
    if (y == current_y)
      return current_layer_data[x + z *n_seqs];
    if (y+1 == current_y)
      return previous_layer_data[x + z *n_seqs];
    assert(0);
  } else {
    // acccess using select on bitmaps_data.
    size_t dict_id = x + z*n_seqs;
    size_t  one_pos = bitmaps_dictionary[dict_id]->select1(1 + y);
    size_t y2 = bitmaps_dictionary[dict_id]->rank1(one_pos) - 1;
    // we substracted the one encoding this number.
    assert(y == y2);
    size_t ans = one_pos - y;  // num of zeros
    return ans;
  }
}

void SuccinctCube::PrepareForQueries() {
  is_modifiable = false;
  assert(current_y == length-1);

  UpdateBitmaps(current_y);
  delete[] previous_layer_data;
  delete[] current_layer_data;

  bitmaps_dictionary = new cds_static::BitSequenceRG*[n_seqs * depth];
  for (uint i = 0; i < n_seqs * depth; i++) {
    bitmaps_dictionary[i] = new cds_static::BitSequenceRG(*bitmaps_data[i], 20);
    for (uint j = 0; j < length; j += 100) {
      int v1 = bitmaps_data[i]->getBit(j);
      int v2 = bitmaps_dictionary[i]->access(j);
      assert(v1 == v2);
    }
  }
}

size_t SuccinctCube::SpaceRequirementBits() {
  return (n_seqs * depth) *
      (length + (uint)max_score + 2*cds_utils::bits((uint)max_score));
}

SuccinctCube::~SuccinctCube() {
  for (uint i = 0; i < n_seqs * depth; i++) {
    delete (bitmaps_data[i]);
  }
  delete []bitmaps_data;

  if (is_modifiable) {
    delete[] current_layer_data;
    delete[] previous_layer_data;
  } else {
    for (uint i = 0; i < n_seqs * depth; i++) {
      delete (bitmaps_dictionary[i]);
    }
    delete []bitmaps_dictionary;
  }
}

