/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include "./score_matrix.h"
#include <cppUtils.h>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <vector>
#include <algorithm>
#include <fstream>
#include "./basic.h"
#include "./debug.h"

ScoreMatrix::ScoreMatrix() {
  n_seqs = 0;
  length = 0;
  scores = NULL;
}

ScoreMatrix::ScoreMatrix(size_t n_sequences, size_t len) {
  n_seqs = n_sequences;
  length = len;
  scores = new score_t[n_seqs * length];
  for (size_t i = 0; i < n_seqs * length; i++) {
    scores[i] = 0;
  }
}

ScoreMatrix::~ScoreMatrix() {
  if (scores != NULL)
    delete[] scores;
}

void ScoreMatrix::Save(char * file_name) {
  std::ofstream of;
  of.open(file_name);
  cds_utils::saveValue(of, n_seqs);
  cds_utils::saveValue(of, length);
  cds_utils::saveValue(of, scores, n_seqs*length);
  of.close();
}

void ScoreMatrix::Load(char * file_name) {
  std::ifstream ifs;
  ifs.open(file_name);
  n_seqs = cds_utils::loadValue<size_t>(ifs);
  length = cds_utils::loadValue<size_t>(ifs);
  scores = cds_utils::loadValue<score_t>(ifs, n_seqs * length);
}

void ScoreMatrix::IncrementScore(size_t sequence, size_t pos, size_t len) {
  assert(pos + len <= length);
  assert(sequence < n_seqs);
  for (size_t j = pos; j < pos + len; j++) {
    scores[sequence * length + j]++;
  }
}

void ScoreMatrix::PrintLevel(PlainCube * matrix, size_t level) {
  for (size_t j = 0; j < length; j++) {
    printf("***");
  }
  printf("\n");
  for (size_t i = 0; i < n_seqs; i++) {
    for (size_t j = 0; j < length; j++) {
      printf("|%i|", matrix->Get(i, j, level));
    }
    printf("\n");
  }
  for (size_t j = 0; j < length; j++) {
    printf("***");
  }
  printf("\n");
}

void ScoreMatrix::Print() {
  for (size_t j = 0; j < length; j++) {
    if(j == 0 || j%10)
      printf("**");
    else {
      size_t ind = j < 100 ? j : j/10;
      printf("%lu", ind);
    }
  }
  printf("\n");
  for (size_t i = 0; i < n_seqs; i++) {
    for (size_t j = 0; j < length; j++) {
      printf("%i-", (this)->Get(i, j));
    }
    printf("\n");
  }
  for (size_t j = 0; j < length; j++) {
    if(j%10)
      printf("**");
    else
      printf("++");
  }
  printf("\n");
}

