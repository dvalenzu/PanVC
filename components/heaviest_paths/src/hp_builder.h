/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef SRC_HEAVIEST_PATH_H_
#define SRC_HEAVIEST_PATH_H_ 
#include <SequenceBuilder.h>
#include "./succinct_cube.h"
#include "./plain_cube.h"
#include "./score_matrix.h"

class HeaviestPath {
 public:
  virtual size_t * RestrictedHeaviestPath(ScoreMatrix * scores,
                                          size_t changes_allowed, 
                                          score_t max_score, 
                                          score_t  * total_weight,
                                          size_t * bytes_required) = 0;
  static size_t * Unrestricted(ScoreMatrix * scores, score_t * total_weight);
 protected:
  void ComputeCube(ScoreMatrix * scores,
                   Cube * cube,
                   size_t changes_allowed);
  size_t * GetFullPath(Cube * matrix,
                       size_t changes_allowed,
                       score_t * total_weight);
  size_t * RedoPartialPath(Cube * cube,
                           size_t * changes_remaining,
                           size_t partial_length,
                           size_t ending_sequence);
};
#endif
