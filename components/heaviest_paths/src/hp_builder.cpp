/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <algorithm>
#include "./hp_builder.h"
#include "./succinct_cube.h"
#include "./cube.h"
#include "./partial_cube.h"
#include "./plain_cube.h"
#include "./score_matrix.h"
#include "./basic.h"

size_t * HeaviestPath::Unrestricted(ScoreMatrix * scores, score_t * total_weight) {
  size_t length = scores->GetLength();
  size_t n_seqs = scores->GetNSeqs();
  size_t * path = new size_t[length];
  size_t max_i = 0;
  score_t tot = 0;
  for (size_t j = 0; j < length; j++) {
    score_t max_score = scores->Get(max_i, j);
    for (size_t i = 0; i < n_seqs; i++) {
      if ((scores)->Get(i, j) > max_score) {
        max_score = (scores)->Get(i, j);
        max_i = i;
      }
    }
    path[j] = max_i;
    tot += max_score;
  }
  *total_weight = tot;
  return path;
}


void HeaviestPath::ComputeCube(ScoreMatrix * scores,
                               Cube * cube,
                               size_t changes_allowed) {
  size_t length = cube->GetLength();
  size_t n_seqs = cube->GetNSeqs();
  for (size_t s = 0; s < n_seqs; s++) {
    cube->Set(s, 0, 0, scores->Get(s, 0));
  }
  for (size_t j = 1; j < length; j++) {
    // no changes allowed:
    for (size_t s = 0; s < n_seqs; s++) {
      cube->Set(s, j, 0, scores->Get(s, j) + cube->Get(s, j-1, 0));
    }
    for (size_t l = 1; l <= std::min(changes_allowed, j); l++) {
      size_t max_seq, second_max_seq;
      score_t max_val, second_max_val;
      cube->TwoMax(j-1,
                   l-1,
                   &max_seq,
                   &second_max_seq,
                   &max_val,
                   &second_max_val);
      for (size_t s = 0; s < n_seqs; s++) {
        // we only compute values:
        score_t alt_val = (s == max_seq) ? second_max_val : max_val;
        cube->Set(s, j, l,
                  scores->Get(s, j) + std::max(cube->Get(s, j-1, l), alt_val));
      }
    }
  }
}

size_t * HeaviestPath::RedoPartialPath(Cube * cube,
                                       size_t * changes_remaining,
                                       size_t partial_length,
                                       size_t ending_sequence) {
  assert(partial_length <= cube->GetLength());
  size_t choosen_seq = ending_sequence;
  size_t * ans = new size_t[partial_length];
  size_t j_ans = partial_length - 1;
  size_t j_cube = cube->GetLength() - 1;
  ans[j_ans] = choosen_seq;
  j_ans--;
  j_cube--;
  while (j_ans != (size_t)-1) {
    if ((*changes_remaining) == 0) {
      while (j_ans != (size_t)-1) {
        ans[j_ans] = choosen_seq;
        j_ans--;
        j_cube--;
      }
      break;
    }
    assert((*changes_remaining) > 0);
    score_t alt_val;
    size_t alt_seq;

    score_t max_val;
    score_t second_max_val;
    size_t max_seq;
    size_t second_max_seq;

    cube->TwoMax(j_cube,
                   (*changes_remaining) - 1,
                   &max_seq,
                   &second_max_seq,
                   &max_val,
                   &second_max_val);
    if (max_seq != choosen_seq) {
      alt_seq = max_seq;
      alt_val = max_val;
    } else {
      alt_seq = second_max_seq;
      alt_val = second_max_val;
    }

    if (alt_val > cube->Get(choosen_seq, j_cube, (*changes_remaining))) {
      choosen_seq = alt_seq;
      (*changes_remaining)--;
    }
    ans[j_ans] = choosen_seq;
    j_ans--;
    j_cube--;
  }
  return ans;
}

size_t * HeaviestPath::GetFullPath(Cube * cube,
                                size_t changes_allowed,
                                score_t * total_weight) {
  size_t length = cube->GetLength();
  size_t choosen_seq;
  cube->Max(length - 1, changes_allowed, &choosen_seq, total_weight);
  return RedoPartialPath(cube, &changes_allowed, length, choosen_seq);
}
