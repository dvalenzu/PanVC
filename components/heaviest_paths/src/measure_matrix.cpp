/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <sys/times.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <cstdlib>
#include <cassert>
#include "./basic.h"
#include "./debug.h"
#include "./utils.h"
#include "./score_matrix.h"
#include "./hp_builder.h"
#include "./hp_builder_plain.h"
#include "./hp_builder_succinct.h"
#include "./hp_builder_recursive.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

void printUssage();
void printUssage() {
  printf("Ussage:\n\n");
  printf("./measure_matrix matrix_score_file min_changes max_changes change_step\n");
}
void measure(HeaviestPath * builder,
             ScoreMatrix * scores,
             size_t changes,
             score_t max_score,
             size_t ** path,
             score_t * weight_path,
             size_t * bytes_used,
             double * time_used);

void measure(HeaviestPath * builder,
             ScoreMatrix * scores,
             size_t changes,
             score_t max_score,
             size_t ** path,
             score_t * weight_path,
             size_t * bytes_used,
             double * time_used) {
  try {
  Utils::StartClock();
  *path = builder->RestrictedHeaviestPath(scores,
                                         changes,
                                         max_score,
                                         weight_path,
                                         bytes_used);
  double t1 = Utils::StopClock();
  *time_used = t1;
  } catch(...) {
    *time_used = 0;
    *bytes_used = 0;
    *path = new size_t[scores->GetLength()];
  }
}

int main(int argc, char ** argv) {
  if (argc != 5) {
    printUssage();
    exit(-1);
  }

  PlainHPBuilder * plain_builder = new PlainHPBuilder();
  SuccinctHPBuilder * succinct_builder = new SuccinctHPBuilder();
  RecursiveHPBuilder* recursive_builder = new RecursiveHPBuilder(0);
  
  char * input_name = argv[1];
  size_t min_changes= (size_t)atoi(argv[2]);
  size_t max_changes= (size_t)atoi(argv[3]);
  size_t changes_step = (size_t)atoi(argv[4]);

  ScoreMatrix * scores = new ScoreMatrix();
  scores->Load(input_name);
  
  size_t length = scores->GetLength();
  size_t n_seqs= scores->GetNSeqs();
  score_t max_score;
  size_t * tmp_path = HeaviestPath::Unrestricted(scores, &max_score);
  delete[] tmp_path;
  
  for (size_t changes = min_changes; changes <= max_changes; changes+=changes_step) {
    printf("Max Allowed changes: %lu\n", changes);
    score_t weight;
    score_t weight_new;
    size_t * path;
    size_t space_requirement;
    double time_used;
    
    measure(plain_builder,
            scores,
            changes,
            max_score,
            & path,
            & weight,
            & space_requirement,
            & time_used);
    // TODO: verify that space is in bits instead f bytes. maybe rename functions
    // to doumnet this fact.
    printf("Plain (time, bits, KB, bpc):\n (%f, %lu, %lu, %lu) .\n",
           time_used,
           space_requirement,
           space_requirement/(8*1024),
           space_requirement/(n_seqs*(changes + 1)*length));
    printf("--------------\n"); 
    delete[] path;

    measure(succinct_builder,
            scores,
            changes,
            max_score,
            & path,
            & weight_new,
            & space_requirement,
            & time_used);
    assert(weight_new == weight);
    // TODO: verify that space is in bits instead f bytes. maybe rename functions
    // to doumnet this fact.
    printf("Succinct (time, bits, KB, bpc):\n (%f, %lu, %lu, %lu) .\n",
           time_used,
           space_requirement,
           space_requirement/(8*1024),
           space_requirement/(n_seqs*(changes + 1)*length));
    printf("--------------\n"); 
    delete[] path;
    
    size_t bits_succinct = space_requirement;
    
    recursive_builder->SetMaxAllowedBits(bits_succinct); 
    measure(recursive_builder,
            scores,
            changes,
            max_score,
            & path,
            & weight_new,
            & space_requirement,
            & time_used);
    assert(weight_new == weight);
    // TODO: verify that space is in bits instead f bytes. maybe rename functions
    // to doumnet this fact.
    printf("Recursive (time, bits, KB, bpc):\n (%f, %lu, %lu, %lu) .\n",
           time_used,
           space_requirement,
           space_requirement/(8*1024),
           space_requirement/(n_seqs*(changes + 1)*length));
    
    assert(space_requirement <= bits_succinct);
    delete[] path;
    printf("--------------\n"); 
    printf("******************\n"); 
  }
  delete(plain_builder);
  delete(succinct_builder);
  delete(recursive_builder);
  delete(scores);
}


