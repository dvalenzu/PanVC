/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef SRC_ARRAY_3D_H_ 
#define SRC_ARRAY_3D_H_ 

#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include "./basic.h"
#include "./cube.h"

class PlainCube : public Cube {
 protected:
  std::vector<score_t> m_data;
 public:
  PlainCube(size_t x, size_t y, size_t z, score_t _max_score, score_t init);
  
  size_t SpaceRequirementBits();
  void Set(size_t x, size_t y, size_t z, score_t val);
  score_t Get(size_t x, size_t y, size_t z);
};

#endif
