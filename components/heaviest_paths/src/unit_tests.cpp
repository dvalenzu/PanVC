/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <BitSequence.h>
#include <BitSequenceRG.h>
#include <sys/times.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include "./basic.h"
#include "./debug.h"
#include "./score_matrix.h"
#include "./plain_cube.h"
#include "./partial_cube.h"
#include "./succinct_cube.h"
#include "./hp_builder.h"
#include "./hp_builder_recursive.h"
#include "./hp_builder_succinct.h"
#include "./hp_builder_plain.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

bool global_fail = false;
void fail();
void success();


void fail() {
  printf(ANSI_COLOR_RED "\tTest Failed\n\n"ANSI_COLOR_RESET);
  global_fail = true;
}
void success() {
  printf(ANSI_COLOR_BLUE "\tSuccess\n\n"ANSI_COLOR_RESET);
}

void testBitmapAccess(uint length, uint n_ones);
void testScoreMatrixLoadSave(size_t n_seq, size_t length);

void testPlainCube(size_t n_sequences, size_t depth, size_t length);
void testPartialCube(size_t n_sequences, size_t depth, size_t length);
void testSuccinctCube(size_t n_sequences, size_t depth, size_t length);

void testHeaviestPathWithoutRestriction();

void genericTestHeaviestPath_A(HeaviestPath * builder);
void genericTestHeaviestPath_B(HeaviestPath * builder);

void testPlainHP();
void testRecursiveHP();
void testSuccinctHP();

void testBitmapAccess(uint length, uint n_ones) {
  printf("testBitmapAccess:\n");
  uint act_ones = 0;
  BitString a(length);
  while (act_ones < n_ones) {
    size_t pos = ((uint)rand()%length);
    if (!a[pos]) {
      act_ones++;
      a.setBit(pos, true);
    }
  }
  cds_static::BitSequenceRG bsRG(a, 20);
  success();
}

void testScoreMatrixLoadSave(size_t n_seq, size_t length) {
  printf("testScoreMatrixLoadSave:\n");
  char *file_name = new char[128];
  strcpy(file_name, "tmp_scores.tmp");

  ScoreMatrix * scores = new ScoreMatrix(n_seq, length);
  for (size_t i = 0; i < n_seq; i++) {
    for (size_t j = 0; j < length; j++) {
      scores->Set(i, j, i*1000+j);
    }
  }
  scores->Save(file_name);
  delete(scores);

  ScoreMatrix * new_scores=  new ScoreMatrix();
  new_scores->Load(file_name);

  for (size_t i = 0; i < n_seq; i++) {
    for (size_t j = 0; j < length; j++) {
      if (new_scores->Get(i, j) != score_t(i * 1000 + j)) {
        fail();
        return;
      }
    }
  }

  delete[]file_name;
  delete(new_scores);
  success();
}


void testPlainCube(size_t n_sequences, size_t depth, size_t length) {
  printf("testPlainCube:\n");
  score_t max_score = 100000;
  score_t initial_score = 0;
  PlainCube matrix(n_sequences, length, depth, max_score, initial_score);
  for (size_t s = 0; s < n_sequences; s++) {
    for (size_t j = 0; j < length; j++) {
      for (size_t l = 0; l < depth; l++) {
        matrix.Set(s, j, l, (score_t)(l*10000 + s*100 + j));
      }
    }
  }

  for (size_t s = 0; s < n_sequences; s++) {
    for (size_t j = 0; j < length; j++) {
      for (size_t l = 0; l < depth; l++) {
        if (matrix.Get(s, j, l) != (int) (l*10000 + s*100 + j)) {
          fail();
          return;
        }
      }
    }
  }
  success();
}

void testPartialCube(size_t n_sequences, size_t depth, size_t length) {
  printf("testPartialCube:\n");
  score_t max_score = 100000;
  score_t initial_score = 0;
  size_t n_layers = 10;
  PartialCube partial_cube(n_sequences,
                           length,
                           depth,
                           n_layers,
                           max_score,
                           initial_score);
  for (size_t j = 0; j < length; j++) {
    for (size_t s = 0; s < n_sequences; s++) {
      for (size_t l = 0; l < depth; l++) {
        partial_cube.Set(s, j, l, (score_t)(l*10000 + s*100 + j));
      }
    }
  }

  for (size_t j = length - n_layers; j < length; j++) {
    for (size_t s = 0; s < n_sequences; s++) {
      for (size_t l = 0; l < depth; l++) {
        if (partial_cube.Get(s, j, l) != (int) (l*10000 + s*100 + j)) {
          fail();
          return;
        }
      }
    }
  }
  success();
}

void testSuccinctCube(size_t n_sequences, size_t depth, size_t length) {
  printf("testSuccinctCube:\n");
  score_t max_score = 100000;
  score_t initial_score = 0;
  SuccinctCube matrix(n_sequences, length, depth, max_score, initial_score);
  for (size_t j = 0; j < length; j++) {
    for (size_t s = 0; s < n_sequences; s++) {
      for (size_t l = 0; l < depth; l++) {
        matrix.Set(s, j, l, (score_t)(l*10000 + s*100 + j));
        if (j > 0) {
          if (matrix.Get(s, j-1, l) != (int) (l*10000 + s*100 + (j-1))) {
            fail();
            return;
          }
        }
      }
    }
  }

  matrix.PrepareForQueries();
  for (size_t s = 0; s < n_sequences; s++) {
    for (size_t j = 0; j < length; j++) {
      for (size_t l = 0; l < depth; l++) {
        if (matrix.Get(s, j, l) != (int) (l*10000 + s*100 + j)) {
          fail();
          return;
        }
      }
    }
  }

  success();
}

void testHeaviestPathWithoutRestriction() {
  printf("testHeaviestPathWithoutRestriction:\n");
  size_t n_sequences = 5;
  size_t length = 10;

  ScoreMatrix * scores = new ScoreMatrix(n_sequences, length);
  // scores->print();

  scores->IncrementScore(0, 1, 5);
  scores->IncrementScore(0, 1, 3);
  scores->IncrementScore(1, 3, 6);
  scores->IncrementScore(2, 3, 5);
  scores->IncrementScore(4, 5, 3);
  scores->IncrementScore(4, 5, 3);
  scores->IncrementScore(4, 0, 10);
  scores->IncrementScore(1, 0, 10);
  scores->IncrementScore(3, 9, 1);
  scores->IncrementScore(3, 9, 1);
  scores->IncrementScore(3, 9, 1);
  // scores->print();
  score_t weight;
  size_t * path = HeaviestPath::Unrestricted(scores, &weight);

  size_t * expected_path = new size_t[length];
  expected_path[0] = 1;
  expected_path[1] = 0;
  expected_path[2] = 0;
  expected_path[3] = 0;
  expected_path[4] = 1;
  expected_path[5] = 4;
  expected_path[6] = 4;
  expected_path[7] = 4;
  expected_path[8] = 1;
  expected_path[9] = 3;

  if (!Debug::compareArrays(expected_path, path, length)) {
    fail();
    return;
  }
  if (weight != 23) {
    fail();
    return;
  }

  delete(scores);
  delete[] path;
  delete[] expected_path;
  success();
}

void genericTestHeaviestPath_A(HeaviestPath * heaviest_path_builder) {
  printf("genericTestHeaviestPath:\n");
  size_t n_sequences = 5;
  size_t length = 10;
  score_t max_score = 10000;
  ScoreMatrix * scores = new ScoreMatrix(n_sequences, length);
  size_t bytes_required;
  // scores->print();

  scores->IncrementScore(0, 1, 5);
  scores->IncrementScore(0, 1, 3);
  scores->IncrementScore(1, 3, 6);
  scores->IncrementScore(2, 3, 5);
  scores->IncrementScore(4, 5, 3);
  scores->IncrementScore(4, 5, 3);
  scores->IncrementScore(4, 0, 10);
  scores->IncrementScore(1, 0, 10);
  scores->IncrementScore(3, 9, 1);
  scores->IncrementScore(3, 9, 1);
  scores->IncrementScore(3, 9, 1);

  // scores->Print();
  score_t weight;
  size_t * path = heaviest_path_builder->RestrictedHeaviestPath(scores,
                                                                1,
                                                                max_score,
                                                                &weight,
                                                                &bytes_required);
  // Debug::printArray(path, length);

  size_t * expected_path = new size_t[length];
  expected_path[0] = 1;
  expected_path[1] = 1;
  expected_path[2] = 1;
  expected_path[3] = 1;
  expected_path[4] = 1;
  expected_path[5] = 1;
  expected_path[6] = 1;
  expected_path[7] = 1;
  expected_path[8] = 1;
  expected_path[9] = 3;

  if (!Debug::compareArrays(expected_path, path, length)) {
    fail();
    return;
  }
  if (weight != 18) {
    fail();
    return;
  }

  delete(scores);
  delete[] path;
  delete[] expected_path;
  success();
}

void genericTestHeaviestPath_B(HeaviestPath * heaviest_path_builder) {
  printf("genericTestHeaviestPath2:\n");
  size_t n_sequences = 4;
  size_t length = 60;
  score_t max_score = 10000;
  size_t bytes_required;

  ScoreMatrix * scores = new ScoreMatrix(n_sequences, length);
  // scores->print();
  for (int i = 0; i < 20; i++) {
    scores->IncrementScore(0, 0, 20);
    scores->IncrementScore(1, 20, 20);
    scores->IncrementScore(0, 40, 20);
  }

  for (int i = 0; i < 25; i++) {
    scores->IncrementScore(2, 10, 3);
    scores->IncrementScore(2, 30, 4);
    scores->IncrementScore(2, 50, 5);
  }

  for (int i = 0; i < 30; i++) {
    scores->IncrementScore(3, 12, 1);
    scores->IncrementScore(3, 32, 1);
    scores->IncrementScore(3, 52, 1);
  }


  // scores->Print();

  // Max changes: 0
  score_t weight_0;
  size_t * path_0 = heaviest_path_builder->RestrictedHeaviestPath(scores,
                                                                  0,
                                                                  max_score,
                                                                  &weight_0,
                                                                  &bytes_required);

  size_t * expected_path_0 = new size_t[length];
  for (uint i = 0; i < length; i++) expected_path_0[i] = 0;

  if (weight_0 != 800) {
    fail();
    return;
  }
  if (!Debug::compareArrays(expected_path_0, path_0, length)) {
    fail();
    return;
  }
  delete[] path_0;
  delete[] expected_path_0;

  // Max changes: 2
  score_t weight_2;
  size_t * path_2 = heaviest_path_builder->RestrictedHeaviestPath(scores,
                                                                  2,
                                                                  max_score,
                                                                  &weight_2,
                                                                  &bytes_required);

  size_t * expected_path_2 = new size_t[length];
  for (uint i = 0; i < length; i++) expected_path_2[i] = 0;
  for (uint i = 20; i < 40; i++) expected_path_2[i] = 1;

  if (weight_2 != 1200) {
    fail();
    return;
  }
  if (!Debug::compareArrays(expected_path_2, path_2, length)) {
    fail();
    return;
  }
  delete[] path_2;
  delete[] expected_path_2;

  // Max changes: 4
  score_t weight_4;
  size_t * path_4 = heaviest_path_builder->RestrictedHeaviestPath(scores,
                                                                  4,
                                                                  max_score,
                                                                  &weight_4,
                                                                  &bytes_required);

  size_t * expected_path_4 = new size_t[length];
  for (uint i = 0; i < length; i++) expected_path_4[i] = 0;
  for (uint i = 20; i < 40; i++) expected_path_4[i] = 1;
  for (uint i = 50; i < 55; i++) expected_path_4[i] = 2;

  if (weight_4 != 1225) {
    fail();
    return;
  }
  if (!Debug::compareArrays(expected_path_4, path_4, length)) {
    fail();
    return;
  }
  delete[] path_4;
  delete[] expected_path_4;

  // Max changes: 8
  score_t weight_8;
  size_t * path_8 = heaviest_path_builder->RestrictedHeaviestPath(scores,
                                                                  8,
                                                                  max_score,
                                                                  &weight_8,
                                                                  &bytes_required);

  size_t * expected_path_8 = new size_t[length];
  for (uint i = 0; i < length; i++) expected_path_8[i] = 0;
  for (uint i = 20; i < 40; i++) expected_path_8[i] = 1;
  for (uint i = 50; i < 55; i++) expected_path_8[i] = 2;
  for (uint i = 30; i < 34; i++) expected_path_8[i] = 2;
  for (uint i = 10; i < 13; i++) expected_path_8[i] = 2;

  if (weight_8 != 1260) {
    fail();
    return;
  }
  if (!Debug::compareArrays(expected_path_8, path_8, length)) {
    fail();
    return;
  }
  delete[] path_8;
  delete[] expected_path_8;

  delete(scores);
  success();
}

void testPlainHP() {
  printf("testPlainHP:\n");
  PlainHPBuilder * path_builder = new PlainHPBuilder();
  genericTestHeaviestPath_A(path_builder);
  genericTestHeaviestPath_B(path_builder);
  delete path_builder;
  success();
}

void testRecursiveHP() {
  printf("testRecursiveHP:\n");
  size_t max_bytes = 0;  // fores a smal num of layers.
  RecursiveHPBuilder * path_builder = new RecursiveHPBuilder(max_bytes);
  genericTestHeaviestPath_A(path_builder);
  genericTestHeaviestPath_B(path_builder);
  delete path_builder;
  success();
}

void testSuccinctHP() {
  printf("testSuccinctHP:\n");
  SuccinctHPBuilder * path_builder = new SuccinctHPBuilder();
  genericTestHeaviestPath_A(path_builder);
  genericTestHeaviestPath_B(path_builder);
  delete path_builder;
  success();
}

int main() {
  testBitmapAccess(100, 20);
  testScoreMatrixLoadSave(20, 10);
  testScoreMatrixLoadSave(5, 10);

  testPlainCube(5, 10, 100);
  testPlainCube(10, 5, 100);
  testPartialCube(5, 10, 100);
  testPartialCube(10, 5, 100);
  testSuccinctCube(5, 10, 100);
  testSuccinctCube(10, 5, 100);

  testHeaviestPathWithoutRestriction();

  testPlainHP();
  testRecursiveHP();
  testSuccinctHP();

  if (!global_fail) {
    printf(ANSI_COLOR_GREEN "\n\tALL UNIT TEST PASSED\n"ANSI_COLOR_RESET);
  } else {
    printf(ANSI_COLOR_RED "\n\tSOME UNIT TESTS FAILED\n"ANSI_COLOR_RESET);
  }
}
