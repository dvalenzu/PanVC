/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <algorithm>
#include "./hp_builder_recursive.h"
#include "./succinct_cube.h"
#include "./cube.h"
#include "./partial_cube.h"
#include "./plain_cube.h"
#include "./score_matrix.h"
#include "./basic.h"

RecursiveHPBuilder::RecursiveHPBuilder(size_t bits_allowed) {
  max_allowed_bits = bits_allowed;
}

void RecursiveHPBuilder::SetMaxAllowedBits(size_t max_bits) {
  max_allowed_bits = max_bits;
}

size_t * RecursiveHPBuilder::RestrictedHeaviestPath(ScoreMatrix * scores,
                                                    size_t changes_allowed,
                                                    score_t max_score,
                                                    score_t  * total_weight,
                                                    size_t * bits_required) {
  *bits_required = max_allowed_bits;  // TODO: compute actual value
  size_t length = scores->GetLength();
  size_t n_seqs = scores->GetNSeqs();
  
  assert(changes_allowed < length);

  size_t max_layers;
  if (max_allowed_bits != 0) {
    size_t bits_per_layer = (changes_allowed + 1)*n_seqs*
        cds_static::bits((uint)max_score);
    printf("Bits Per Layer: %i\n", (int)bits_per_layer);
    max_layers = (max_allowed_bits + bits_per_layer - 1)/bits_per_layer;
  } else {
    max_layers = 4;
  }
  printf("We will use at most %lu layers.\n", max_layers);

  size_t current_choosen_seq = (size_t)-1;
  size_t changes_remaining = changes_allowed;

  size_t * ans = new size_t[length];
  while (length > 0) {
    if (length < max_layers) {
      max_layers = length;   // TOOD: be sure this is correct. (it was  + 1)
    }
    score_t default_score = 0;
    // optimized for this problem:
    /*
    PartialCube partial_cube(n_seqs,
                             length,
                             changes_remaining + 1,
                             max_layers,
                             max_score,
                             default_score);
    ComputeCube(scores, &partial_cube, changes_remaining);
    */
    PartialCube partial_cube(n_seqs,
                             length,
                             changes_allowed + 1,
                             max_layers,
                             max_score,
                             default_score);
    ComputeCube(scores, &partial_cube, changes_allowed);
    if (current_choosen_seq == (size_t)-1) {
      partial_cube.Max(length - 1,
                       changes_allowed,
                       &current_choosen_seq,
                       total_weight);
    }
    size_t * partial_path = RedoPartialPath(&partial_cube,
                                            &changes_remaining,
                                            max_layers,
                                            current_choosen_seq);

    for (uint i = 1; i <= max_layers; i++) {
      ans[length - i] = partial_path[max_layers - i];
      // printf("Filling ans[%i] = %i\n", (int)(length-i), (int)ans[length-i]);
      if (length == i) break;
    }
    if (length == max_layers ) {
      delete[] partial_path;
      break;
    } 
    length = length - max_layers + 1;
    current_choosen_seq = partial_path[0];
    delete[] partial_path;
  }
  return ans;
}
