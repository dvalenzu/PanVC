#!/usr/bin/env bash
set -o errexit
set -o nounset
#set -x
#trap read debug

#
# Extract the desired individual sequences, and index them with BWA
#


if [ "$#" -ne 4 ]; then
  echo "Illegal number of parameters. I use 4 params instead of $# " 
  exit 33
fi
  echo "OK Params"

SRC_SEQ_FOLDER=$1
PAN_REFERENCE_FOLDER=$2
FIRST_REF=$3
LAST_REF=$4


if [ -d "$PAN_REFERENCE_FOLDER" ]; then
  echo "Folder: $PAN_REFERENCE_FOLDER exitsts, if you want to override, remove it."
  echo "We will skip indexing and reuse it."
  exit
fi

mkdir  -p ${PAN_REFERENCE_FOLDER}

echo "**********"
echo "Folders prepared"
echo "**********"


# DEPENDS ON:
# sam_to_positions.py
# reads_to_fastq.py for the exact reads only
# gap_positions.py

#Defines BWA_BIN
source ../../ext/set_env.sh

if [ ! -f "${BWA_BIN}" ]; then
  echo "File '${BWA_BIN}'not found!. File: '${0}'"
  exit 33
fi

for (( CURRENT_REFERENCE=$FIRST_REF; CURRENT_REFERENCE<=$LAST_REF; CURRENT_REFERENCE++ ))
do
  # Make all index:

  SEQUENCE_FULL=$PAN_REFERENCE_FOLDER/recombinant.n$CURRENT_REFERENCE.full
  SEQUENCE_FILE=$PAN_REFERENCE_FOLDER/recombinant.n$CURRENT_REFERENCE.fa
  echo ">$SEQUENCE_FILE" > $SEQUENCE_FILE
  zcat $SRC_SEQ_FOLDER/all.maligned.gz |awk '{printf("%s", substr($0,'$CURRENT_REFERENCE',1))}' > $SEQUENCE_FULL
  cat $SEQUENCE_FULL | tr -d '-' >> $SEQUENCE_FILE
  GAP_POS_FILE=$PAN_REFERENCE_FOLDER/recombinant.n$CURRENT_REFERENCE.gap_positions
  python gap_positions.py $SEQUENCE_FULL > $GAP_POS_FILE

  $BWA_BIN index $SEQUENCE_FILE #&
done
wait

