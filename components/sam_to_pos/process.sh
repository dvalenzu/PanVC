#!/usr/bin/env bash
set -o errexit
set -o nounset

if [ "$#" -ne 3 ]; then
  echo "Illegal number of parameters. I should be called from the pipeline script, with 2 params instead of $# " 
  exit 33
else
  echo "OK Params"
fi

PAN_REFERENCE_FILE=$1
SAM_FOLDER=$2
SENSIBILITY=$3
if [ ! -f $1 ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${1}'. Quitting. "
  exit 33
fi
if [ ! -d $2 ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${2}'. Quitting. "
  exit 33
fi

echo "Sam folder is: ${SAM_FOLDER}"

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source ${DIR}/config.sh

#######################################################
#TODO change names Ref -> tmp, except last.
REF_FOLDER=$(dirname "${PAN_REFERENCE_FILE}")
REF_BASE=`basename $PAN_REFERENCE_FILE`
REF_PREFIX=${REF_BASE%.*}
PAN_REFERENCE_FOLDER=${REF_FOLDER}/${REF_PREFIX}.index/
#######################################################
N_REFS=$( zcat ${PAN_REFERENCE_FILE}  | tail -n1 | wc -c)
N_REFS=`expr $N_REFS - 1`
echo "Reference contains ${N_REFS} individuals"
#######################################################

for (( CURRENT_REFERENCE=1; CURRENT_REFERENCE<=$N_REFS; CURRENT_REFERENCE++ ))
do
  SEQ_FILE=$PAN_REFERENCE_FOLDER/recombinant.n$CURRENT_REFERENCE.fa
  SAM_FILE=${SAM_FOLDER}/mapped_reads_to${CURRENT_REFERENCE}.sam.gz
  POSITIONS_FILE=${SAM_FOLDER}/mapped_reads_to${CURRENT_REFERENCE}.pos
  python $SAM_TO_POS_PY $SAM_FILE $SEQ_FILE $SENSIBILITY > $POSITIONS_FILE 
# TODO make it optional ? 
#  echo "Removing sam file"
#  rm $SAM_FILE
done
