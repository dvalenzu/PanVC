/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef SRC_UTILS_H
#define SRC_UTILS_H
#include <cstdlib>
#include <cstring>
#include <vector>
#include <string>

using std::vector;
using std::min;
typedef unsigned char uchar;

typedef vector<size_t> list_t;
//typedef std::basic_string <unsigned char> qgram_t;
typedef std::string qgram_t;


class Utils {
 public:
  static bool Coin();
  static uchar RandomChar();
  static void GenerateAlignment(uchar* BASE, size_t BASE_len, uchar **A, uchar **B, size_t * len);
  static  bool SameUnderlyingSequence(uchar * A, size_t A_len, 
                                      uchar * B, size_t B_len);

  static void ProjectAlignment(uchar * A,
                               uchar * B,
                               size_t A_len,
                               uchar * X,
                               uchar * Y,
                               size_t X_len,
                               uchar ** ansA,
                               uchar ** ansY,
                               size_t * new_len);
  static void StartClock();
  static double StopClock();
  static uchar * LoadUcharFile(char * file_name, size_t * ans_len);
  static void AbortPrint(const char* format, ...);
/*
  template<typename TYPE>
static void DumpSeq(TYPE * seq, size_t length, const char * file_name) {
  FILE * file_seq;
  file_seq = fopen(file_name, "w");
  if (file_seq == NULL) {
    Utils::AbortPrint("Could not open file: '%s' for saving.\n", file_name);
  }
  if (fwrite(seq, sizeof(TYPE), length, file_seq) != (length)) Utils::AbortPrint("Error during fwrite.\n)");
  fclose(file_seq);
}
*/
// text utils:
static vector<size_t> NaiveFind(uchar * text,
                                size_t text_length,
                                uchar * pattern,
                                size_t pattern_length);
static void preBmBc(uchar *pattern, size_t m, size_t bmBc[], size_t ASIZE);
static vector<size_t>  Horspool(uchar *text, size_t text_length, uchar *pattern, size_t pattern_length);

static inline void Horspool(uchar *text,
                            size_t ini,
                            size_t end,
                            uchar *pattern,
                            size_t pattern_len,
                            size_t * bmBc,
                            vector<size_t> * ans) {
  /* Searching */
  size_t j;
      uchar c;
      j = ini;
      while (j <= end + 1 - pattern_len) {
        c = text[j + pattern_len - 1];
        if (pattern[pattern_len - 1] == c && memcmp(pattern, text + j, pattern_len - 1) == 0) {
          ans->push_back(j);
        }
        j += bmBc[(int)c];
      }
    }

    static inline size_t HorspoolFirstOcc(uchar *text,
                                        size_t ini,
                                        size_t tentative_end,
                                        uchar *pattern,
                                        size_t pattern_len,
                                        size_t * bmBc) {
      // assert(tentative_end == text_len);
      // Well, we don't see text_len here, thats' why we ask the tentative end.
      size_t j;
      uchar c;
      j = ini;
      while (j <= tentative_end + 1 - pattern_len) {
        c = text[j + pattern_len - 1];
        if (pattern[pattern_len - 1] == c && memcmp(pattern, text + j, pattern_len - 1) == 0) {
          return j;
        }
        j += bmBc[(int)c];
      }
      assert(0);
      return (size_t)-1;
    }

    static inline size_t bits(size_t n) {
      size_t ans = 0;
      while (n > 0) {
        n >>=1;
        ans++;
      }
      return ans;
    }
};
#endif
