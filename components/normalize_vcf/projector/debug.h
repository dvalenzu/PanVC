/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef DEBUG_H
#define DEBUG_H

#include <iostream>

class Debug {
 public:
  template<typename TYPE>
static void PrintArray(TYPE *A, size_t length) {
  if (length == 0) {
    std::cout << "[]";
    return;
  }
  std::cout << "[";
  for (size_t i = 0; i < length-1; i++) {
    //printf("%lu|", A[i]);
    std::cout << "|" << A[i] << "|";
  }
  std::cout << A[length-1] << "]" << std::endl;
}

template<typename TYPE>
static bool EqualArrays(TYPE *A, TYPE  * B, size_t length) {
  for (size_t i = 0; i < length; i++) {
    if (A[i] != B[i]) {
      PrintArray(A, length);
      printf("\n");
      PrintArray(B, length);
      printf("Diff in pos %u \n", (uint)i);
      return false;
    }
  }
  return true;
}


};
#endif  // TOOLS_DEBUG_H
