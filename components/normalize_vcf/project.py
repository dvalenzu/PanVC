#!/usr/bin/env python
import sys
import re
import StringIO
from Bio import SeqIO


n_args = len(sys.argv);
if(n_args != 4):
  print 'Number of arguments:', n_args, 'arguments, is incorrect:'
  sys.exit();
NEW_ALIGNMENT=sys.argv[1];
ADHOC_REF=sys.argv[2];
STD_REF=sys.argv[3];

#############
with open(NEW_ALIGNMENT, 'r') as f:
    A = f.readline()
    B = f.readline()

with open(ADHOC_REF, 'r') as f:
    X = f.readline()

with open(STD_REF, 'r') as f:
    Y = f.readline()

print 'alignmetn projected!'
