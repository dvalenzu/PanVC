#!/usr/bin/env python
import sys
import re
import StringIO
from Bio import SeqIO

# return 1/0
def underlyng_seqs_equals(seq_1, seq_2):
  clean_1=""
  clean_2=""
  for c in seq_1:
    if(c != '-'):
      clean_1+=c
  for c in seq_2:
    if(c != '-'):
      clean_2+=c
  
  if (clean_1 == clean_2):
    return 1
  else:
    i = 0
    for c in clean_1:
      if(c != clean_2[i]):
        print 'error in pos', i , 'quitting,'
        print 'because:', clean_1[i] , 'not eq' , clean_2[i] , 'bye'
        return 0
      else:
        i = i+1
    return 0

n_args = len(sys.argv);
if(n_args != 3):
  print 'Number of arguments:', n_args, 'arguments, is incorrect:'
  sys.exit();
INPUT_FIE=sys.argv[1];
VCF_FILE_NAME=sys.argv[2];

#### IF FASTA:::
records=list(SeqIO.parse(INPUT_FIE, "fasta"));
my_seq=records[0].seq;
#############
#with open(INPUT_FIE, 'r') as f:
#    my_seq= f.readline()


#print(seq_ref.id)
#print(repr(seq_ref.seq))
#print(len(seq_ref))

## OPEN VCF and modify A and B accordingly

vcf_file = open(VCF_FILE_NAME);
ok = 0;
problem = 0;
many_vars = 0;

marker = 0;
new_a = "";
new_b = "";

redundant_vars = 0;
low_qual = 0;
deletions = 0;

for line in vcf_file:
  if line.startswith("#"):
    continue;
  values = line.split('\t');
  pos=int(values[1]) - 1;
  ref=values[3];
  var=values[4];
  qual=values[5];
  status=values[6];
  
  if (qual == "."):
    qual = "0"
  if (float(qual) < 2.0 and status != "PASS"):
    low_qual = low_qual + 1;
    continue

  curr_len = len(ref)
  ref_original = my_seq[pos:pos+curr_len];
  


  if (pos < marker):
    sys.stderr.write('\nSkip a line (redundant var)\n')
    #sys.stderr.write(line)
    redundant_vars = redundant_vars + 1
    #assert(False);
    continue

  if "," in var: 
    many_vars = many_vars + 1;
    continue
  
  if (str(ref_original) != ref):
    sys.stderr.write('In pos: '+str(pos)+' ref is '+ref+' and var is: '+ var+' extract ref is: '+ref_original);
    problem = problem + 1;
    sys.stderr.write('Problem with file:'+ VCF_FILE_NAME+' and with file:'+INPUT_FIE+'\n');
    assert(False);
  # We are now in the ok case: 
  ok = ok + 1;
  new_a += str(my_seq[marker:pos]);
  new_b += str(my_seq[marker:pos]);
    
  token_a = ref;
  token_b = var;
  
  if (len(ref) > len(var)):
    token_b += "-" * (len(ref) - len(var));   # we do want an alignment again!
    deletions = deletions + 1;
  elif (len(ref) < len(var)):
    token_a += "-" * (len(var) - len(ref));
  else:
    assert(len(var) == len(ref));
  
  new_a += token_a;
  new_b += token_b;
  
  marker = pos + len(ref);

# Tail of the file
new_a += str(my_seq[marker:]);
new_b += str(my_seq[marker:]);
sys.stderr.write('Low Qual lines skipped: '+str(low_qual)+'\n')
sys.stderr.write('Redundant lines skipped: '+str(redundant_vars)+'\n')
sys.stderr.write('Many Variatios lines skipped: '+str(many_vars)+'\n')
sys.stderr.write('Vars applied : '+str(ok)+'\n')
#print 'Ok:', ok , 'problem:', problem, 'many variations', many_vars;


## The following is for debug only, as it will slow the code.
## Also this verification is done afterwards in bash and in the projector code.

#if (underlyng_seqs_equals(my_seq, new_a) == 1):
#  sys.stderr.write('[apply_vcf.py] Underlyng seqs consistent, Im happy \n');
#else:
#  sys.stderr.write('[apply_vcf.py] I FAILED, and changed original seq. CHECK apply_vcf.py \n');
#  sys.exit(21); 

#print '>A';
print new_a;
#print '>B\'';
print new_b;

vcf_file.close();
