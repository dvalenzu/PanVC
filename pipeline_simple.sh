#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

source "./utils.sh"

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
SENSIBILITY=5
MAX_RECOMB=0

if [ "$#" -ne 2 ]; then
  echo "Script: '${0}'"
  echo "uses 2 params instead of $#" 
  exit
fi

utils_assert_file_exists ${1}
utils_assert_file_exists ${2}

PAN_GENOME_REF=`readlink -f ${1}`
READS_FILE=`readlink -f ${2}`

WORKING_FOLDER=${DIR}/simple_tmp_output/
mkdir -p ${WORKING_FOLDER}

./pipeline_parametric.sh ${PAN_GENOME_REF} ${READS_FILE} ${WORKING_FOLDER}

mv ${WORKING_FOLDER}/variations.vcf .
rm -rf ${WORKING_FOLDER}

#utils_success_exit
