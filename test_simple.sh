#!/usr/bin/env bash
set -o errexit
set -o nounset

./pipeline_simple.sh ./example_data/reference_pg_aligned.gz ./example_data/reads.fq
