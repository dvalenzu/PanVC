#!/usr/bin/env bash
set -o errexit
set -o nounset

WORKING_FOLDER="./tmp_parametric_test"
rm -rf ${WORKING_FOLDER}
mkdir  -p ${WORKING_FOLDER}
./clean.sh

./pipeline_parametric.sh ./example_data/reference_pg_aligned.gz ./example_data/reads.fq ${WORKING_FOLDER}
